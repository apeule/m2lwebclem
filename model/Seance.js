class Seance{

    constructor(unCode, uneDateSeance, unNbMax, unIdAct, uneActivite){
        this._unCode = unCode;
        this._uneDateSeance = uneDateSeance;
        this._unNbMax = unNbMax;
        this._uneActivite = uneActivite;
        this._unIdAct = unIdAct;
    }


    get unCode() {
        return this._unCode;
    }

    set unCode(value) {
        this._unCode = value;
    }

    get uneDateSeance() {
        return this._uneDateSeance;
    }

    set uneDateSeance(value) {
        this._uneDateSeance = value;
    }

    get unNbMax() {
        return this._unNbMax;
    }

    set unNbMax(value) {
        this._unNbMax = value;
    }

    get uneActivite() {
        return this._uneActivite;
    }

    set uneActivite(value) {
        this._uneActivite = value;
    }

    get unIdAct() {
        return this._unIdAct;
    }

    set unIdAct(value) {
        this._unIdAct = value;
    }
}

module.exports = Seance;