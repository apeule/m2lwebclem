const pg = require('pg');
const Activite = require('../model/Activite');

class ActivitePgDAO {

    constructor(){
    }

    getMesInscriptions(numsecu,displaycb){
        const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        const query = {
            name: 'fetch-activites-inscription',
            text: 'SELECT identifiant, designation, a.nbmax\n' +
            'FROM activite a, participer p, seance sean, senior sen\n' +
            'where a.identifiant = sean.code\n' +
            'and sean.code = p.codeseance\n' +
            'and sen.numsecu = p.numsecu\n'+
            'and sen.numsecu = $1',
            values: [numsecu]
        };

        db.query(query, function (err, result) {
                let listeInscriptions = [];
                if (err) {
                    console.log(err.stack);

                } else {
                    result.rows.forEach(function(row) {
                        let uneActivite;

                        uneActivite = new Activite(row['identifiant'], row['designation'], row['nbmax']);
                        listeInscriptions.push(uneActivite);
                    });
                    displaycb(listeInscriptions);
                }
                db.end();
            }
        );

    }
}


module.exports = ActivitePgDAO;