class Activite{

    constructor(unIdentifiant, uneDesignation, unNbMax){
        this._uneDesignation = uneDesignation;
        this._unIdentifiant = unIdentifiant;
        this._unNbMax = unNbMax;
        this._lesSeances = [];

    }

    ajoutSeance(uneSeance){
        if (uneSeance.unIdAct === this._unIdentifiant){
            this._lesSeances.push(uneSeance);
        }

    }

    get uneDesignation() {
        return this._uneDesignation;
    }

    get unIdentifiant() {
        return this._unIdentifiant;
    }

    get unNbMax() {
        return this._unNbMax;
    }


    set unIdentifiant(value) {
        this._unIdentifiant = value;
    }

    set uneDesignation(value) {
        this._uneDesignation = value;
    }

    set unNbMax(value) {
        this._unNbMax = value;
    }
}

module.exports=Activite;