class Participer{

    constructor(unNumSecu, unCodeSeance){
        this._unNumSecu = unNumSecu;
        this._unCodeSeance = unCodeSeance;
    }


    get unNumSecu() {
        return this._unNumSecu;
    }

    set unNumSecu(value) {
        this._unNumSecu = value;
    }

    get unCodeSeance() {
        return this._unCodeSeance;
    }

    set unCodeSeance(value) {
        this._unCodeSeance = value;
    }
}

module.exports = Participer;