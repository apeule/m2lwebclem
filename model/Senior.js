class Senior{

    constructor(unNumSecu, unNom, uneDateNaissance, unEmail, unPassword){
        this._unNumSecu = unNumSecu;
        this._unNom = unNom;
        this._uneDateNaissance = uneDateNaissance;
        this._unEmail = unEmail;
        this._unPassword = unPassword;
    }


    get unNumSecu() {
        return this._unNumSecu;
    }

    set unNumSecu(value) {
        this._unNumSecu = value;
    }

    get unNom() {
        return this._unNom;
    }

    set unNom(value) {
        this._unNom = value;
    }

    get uneDateNaissance() {
        return this._uneDateNaissance;
    }

    set uneDateNaissance(value) {
        this._uneDateNaissance = value;
    }

    get unEmail() {
        return this._unEmail;
    }

    set unEmail(value) {
        this._unEmail = value;
    }

    get unPassword() {
        return this._unPassword;
    }

    set unPassword(value) {
        this._unPassword = value;
    }
}

module.exports = Senior;