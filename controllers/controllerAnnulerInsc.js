const pg = require('pg');

exports.getLesInscriptions=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();



        const query = {
            name: 'fetch-activites-inscription',
            text: 'SELECT a.identifiant, designation, dateseance, codeseance\n' +
            'FROM activite a, participer p, seance sean, senior sen\n' +
            'where a.identifiant = sean.code\n' +
            'and sean.code = p.codeseance\n' +
            'and sen.numsecu = p.numsecu\n'+
            'and sen.numsecu = $1',
            values: [req.session.user['numsecu']]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('annulerInsc', {listeDesActivitesInscriptions: result.rows,user:req.session.user});
                }
                db.end();
            }
        );

    }
};

exports.annulerInsc=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite',
            text: 'DELETE from participer ' +
            'where participer.numsecu =$1 ' +
            'and participer.codeseance=$2',
            values: [req.session.user['numsecu'], req.body['Supp']]
        };

        /*var cible = document.getElementsByName('Supp').checked;
        if (cible==true){
            cible.selectedIndex
        }*/

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('annulerInsc', {listeDesActivitesInscriptions: result.rows,user:req.session.user});

                    //res.redirect('/annulerInsc');
                }
                db.end();
            }
        );
    }
};