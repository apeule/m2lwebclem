const pg = require('pg');

const ActivitePgDAO = require('../model/ActivitePgDAO');

const activitePgDAO = new ActivitePgDAO();

exports.getAllActivites=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite',
            text: 'SELECT * FROM activite'
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('activite', {listeDesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};

// GET LES INSCRIPTIONS AUX ACTIVITES
exports.getMesInscriptions=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');

    else
        activitePgDAO.getMesInscriptions(req.session.user['numsecu'],
            function(lesInscriptions){

                res.render('programmesante', { listeDesActivitesInscriptions: lesInscriptions})
            }
        );

};


//UPDATE STATUT DE L'ACTIVITE PAR RAPPORT AU SENIOR
exports.updateMonStatut=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'upadate-activite',
            text: 'update participer set statut = true where codeseance = $1',
            values:[codeseance]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);

                } else {
                    res.render('activite', {listeDesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};



