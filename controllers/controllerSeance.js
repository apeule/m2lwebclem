const pg = require('pg');

exports.chercherSeances = function(req, res, next) {


    const connectionString = 'postgres://apeule:apeule@192.168.222.86:5432/M2L';

    const db = new pg.Client(connectionString);
    db.connect();

    const query = {
        name: 'fetch-activites-inscription',
        text: 'SELECT designation,  dateseance\n' +
        'FROM activite a, participer p, seance sean, senior sen\n' +
        'where a.identifiant = sean.code\n'+
        'and sean.code = p.codeseance\n'+
        'and sen.numsecu = p.numsecu\n'+
        'and sen.numsecu = $1',
        values: [req.session.user['numsecu']]
    };

    db.query(query,
        function(err, result){
            if (err) {
                console.log(err.stack);

            } else {

                res.render('seances', {listeDesSeances: result.rows, user:req.session.user});
            }
            db.end();
        }
    );

};
