var express = require('express');
var router = express.Router();

var controllerIndex = require("../controllers/controllerIndex");
var controllerActivite = require("../controllers/controllerActivite");
var controllerAnnulerInsc = require("../controllers/controllerAnnulerInsc");
var controllerSeance = require("../controllers/controllerSeance");

/* GET Connexion page. */
router.get('/', controllerIndex.connexion);
router.post('/', controllerIndex.verificationConnexion);

router.get('/logout', controllerIndex.deconnexion);

router.get('/index', controllerIndex.index);

router.get('/profile', controllerIndex.getProfilUser);



/* ACTIVITES*/
router.get('/activites',controllerActivite.getAllActivites);



/*PROGRAMME SANTE PAGE*/
router.get('/programmesante', controllerActivite.getMesInscriptions);



/*SEANCE PAGE*/
router.get('/seances', controllerSeance.chercherSeances);



/*ANNULER UNE INSCRIPTION*/
router.get('/annulerInsc', controllerAnnulerInsc.getLesInscriptions);
router.post('/annulerInsc', controllerAnnulerInsc.annulerInsc);

//

module.exports = router;
